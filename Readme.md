## WebRTC Test

### Install & Run

```
composer install
php server.php # runs on port 8080
php -S localhost:8081 index2.php
```

### Resources used

- https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection
- https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API

*index.php*
- https://github.com/Choi-Heesu/mediaRecorder-webSocket-mediaSource/blob/main/frontend/js/main.js
- https://github.com/webrtc/samples/blob/gh-pages/src/content/peerconnection/multiple/js/main.js

*index2.php*
- https://github.com/kolson25/WebRTC-Multi-Peer-Video-Audio/blob/master/client/webrtc.js
