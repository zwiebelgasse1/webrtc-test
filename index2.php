<?php

?>

<html>
<head>
    <style>
        .videos {
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
            grid-gap: 0.5rem;
        }

        .video {
            display: inline-block;
            background-color: #ebebeb;
            position: relative;
            border: 1px solid #ebebeb;
        }
        .video:before {
            content: attr(data-id);
            position: absolute;
            top: 1rem; left: 1rem;
            background-color: #fff;
            padding: 0.5rem;
        }

        video {
            width: 100%;
        }
    </style>
</head>
<body>
<video id="video" style="display: none;" autoplay></video>
<div id="videos" class="videos"></div>

<script>
  const localVideo = document.querySelector('#video');
  const videos = document.querySelector('#videos');
  let connections = {};

  init();

  async function init() {
    const localStream = await navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true,
    });

    localVideo.srcObject = localStream;

    const ws = new WebSocket('ws://localhost:8080');
    ws.onmessage = async (ev) => {
      const data = JSON.parse(ev.data);
      console.log(data);

      if (data.type === 'user.joined') {
        for (const clientId of data.clients) {
          if (!connections[clientId]) {
            connections[clientId] = new RTCPeerConnection({
              iceServers: [{ urls: 'stun:localhost:8080' }],
            });

            connections[clientId].onicecandidate = (ev) => {
              if (ev.candidate) {
                ws.send(JSON.stringify({
                  type: 'ice.candidate',
                  candidate: ev.candidate,
                  fromId: clientId,
                }));
              }
            };

            connections[clientId].ontrack = (ev) => {
              let remoteVideo = document.querySelector(`[data-id="${clientId}"]`);
              if (!remoteVideo) {
                remoteVideo = document.createElement('video');
                remoteVideo.autoplay = true;

                const remoteVideoContainer = document.createElement('div');
                remoteVideoContainer.dataset.id = clientId;
                remoteVideoContainer.classList.add('video');
                remoteVideoContainer.appendChild(remoteVideo);
                videos.appendChild(remoteVideoContainer);
              }

              remoteVideo.srcObject = ev.streams[0];
            };

            localStream.getTracks().forEach(track => connections[clientId].addTrack(track, localStream));
          }
        }

        if (data.clients.length > 1) {
          const offer = await (await awaitConnection(data.id)).createOffer();
          await connections[data.id].setLocalDescription(offer);
          ws.send(JSON.stringify({
            type: 'rtc.offer',
            fromId: data.id,
            sdp: offer,
          }));
        }
      }

      else if (data.type === 'user.left') {
        const video = document.querySelector(`[data-id="${data.id}"]`);
        video && video.parentElement.removeChild(video);
        delete connections[data.id];
      }

      else if (data.type === 'rtc.offer') {
        await (await awaitConnection(data.fromId)).setRemoteDescription(new RTCSessionDescription(data.sdp));

        const answer = await connections[data.fromId].createAnswer();
        await connections[data.fromId].setLocalDescription(answer);

        ws.send(JSON.stringify({
          type: 'rtc.answer',
          fromId: data.fromId,
          sdp: answer,
        }));
      }

      else if (data.type === 'rtc.answer') {
        try {
          await (await awaitConnection(data.fromId)).setRemoteDescription(new RTCSessionDescription(data.sdp));
        } catch (e) {
          console.log(e);
        }
      }

      else if (data.type === 'ice.candidate') {
        const candidate = new RTCIceCandidate(data.candidate);
        try {
          await (await awaitConnection(data.fromId)).addIceCandidate(candidate);
        } catch (e) {
          console.log(e);
        }
      }
    };
  }

  async function awaitConnection(id) {
    if (!connections[id]) {
      return awaitConnection(id);
    } else {
      return Promise.resolve(connections[id]);
    }
  }
</script>
</body>
</html>
