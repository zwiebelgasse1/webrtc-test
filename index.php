<?php

?>

<html>
<head></head>
<body>
<video id="video" style="border: 1px solid red;" autoplay></video>
<div id="videos" style="border:1px solid blue;"></div>
<button id="offer" onclick="createOffer()">Send Offer</button>

<script>
  let conn;
  let rtcPeerConnection;
  const video = document.querySelector('#video');
  const videos = document.querySelector('#videos');

  conn = new WebSocket('ws://localhost:8080');
  conn.onopen = function (ev) {
    console.log('established', ev);
  }

  conn.onmessage = function (ev) {
    //console.log(ev);
    const data = JSON.parse(ev.data);
    console.log(data);
    if (data.type === 'offer') {
      handleOffer(data);
    }
    else if (data.type === 'answer') {
      rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(data));
    }
    else if (data.candidate) {
      const candidate = new RTCIceCandidate(data);
      rtcPeerConnection.addIceCandidate(candidate);
    }
  }

  function createOffer() {
    createRTCPeerConnection();

    navigator.mediaDevices.getUserMedia({
      audio: true,
      video: {
        width: 150, height: 150,
      },
    }).then((res) => {
      window.stream = res;

      video.srcObject = window.stream;
      window.stream.getTracks().forEach(track => rtcPeerConnection.addTrack(track, window.stream));
    });
  }

  function handleOffer(data) {
    createRTCPeerConnection();

    rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(data)).then(() => {
      return navigator.mediaDevices.getUserMedia({audio: true, video: {
          width: 150, height: 150,
        }});
    })
    .then((stream) => {
      window.stream = stream;

      video.srcObject = window.stream;
      window.stream.getTracks().forEach(track => rtcPeerConnection.addTrack(track, window.stream));
    })
    .then(() => {
      return rtcPeerConnection.createAnswer();
    })
    .then((answer) => {
      return rtcPeerConnection.setLocalDescription(answer);
    })
    .then(() => {
      conn.send(JSON.stringify(rtcPeerConnection.localDescription.toJSON()));
    })
  }

  function createRTCPeerConnection() {
    rtcPeerConnection = new RTCPeerConnection({
      iceServers: [{ urls: 'stun:localhost:8080' }],
    });

    rtcPeerConnection.onnegotiationneeded = (ev) => {
      rtcPeerConnection.createOffer().then((offer) => {
        return rtcPeerConnection.setLocalDescription(offer);
      })
      .then(() => {
        conn.send(JSON.stringify(rtcPeerConnection.localDescription.toJSON()));
      });
    }

    rtcPeerConnection.onicecandidate = (ev) => {
      if (ev.candidate) {
        conn.send(JSON.stringify(ev.candidate));
      }
    };

    rtcPeerConnection.ontrack = (ev) => {
      console.log('track?', ev);
      const stream = ev.streams[0];
      const id = stream.id.replace(/[{}0-9\-]/g, '');
      let remote = document.querySelector('#' + id);
      if (!remote) {
        remote = document.createElement('video')
        remote.id = id;
        remote.autoplay = true;
        videos.appendChild(remote);
      }
      remote.srcObject = stream;
    };
  }

  /**
   * myPeerConnection.onicecandidate = handleICECandidateEvent;
   myPeerConnection.ontrack = handleTrackEvent;
   myPeerConnection.onnegotiationneeded = handleNegotiationNeededEvent;
   myPeerConnection.onremovetrack = handleRemoveTrackEvent;
   myPeerConnection.oniceconnectionstatechange = handleICEConnectionStateChangeEvent;
   myPeerConnection.onicegatheringstatechange = handleICEGatheringStateChangeEvent;
   myPeerConnection.onsignalingstatechange = handleSignalingStateChangeEvent;
   */
</script>
</body>
</html>
