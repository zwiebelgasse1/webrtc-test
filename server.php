<?php

require 'vendor/autoload.php';

class Chat implements \Ratchet\WebSocket\MessageComponentInterface
{
    protected SplObjectStorage $clients;

    public function __construct() {
        $this->clients = new SplObjectStorage();
    }

    function onOpen(\Ratchet\ConnectionInterface $conn)
    {
        $this->clients->attach($conn);

        echo "PHP: New connection! ({$conn->resourceId})\n";

        $conn->send(json_encode([
            'type' => 'user.joined',
            'id' => $conn->resourceId,
            'clients' => $this->getClientIds(),
        ]));

        foreach ($this->clients as $client) {
            if ($client !== $conn) {
                $client->send(json_encode([
                    'type' => 'user.joined',
                    'id' => $conn->resourceId,
                    'clients' => $this->getClientIds(),
                ]));
            }
        }
    }

    function onClose(\Ratchet\ConnectionInterface $conn)
    {
        $this->clients->detach($conn);

        echo "PHP: Disconnected connection! ({$conn->resourceId})\n";

        foreach ($this->clients as $client) {
            $client->send(json_encode([
                'type' => 'user.left',
                'id' => $conn->resourceId,
            ]));
        }
    }

    function onError(\Ratchet\ConnectionInterface $conn, \Exception $e)
    {
        echo "PHP: ERROR: ({$e->getMessage()})\n";
        $conn->close();
    }

    function onMessage(\Ratchet\ConnectionInterface $from, $msg)
    {
        echo "PHP: Connection $from->resourceId sending message to ppl!\n";
        if (is_string($msg)) {
            var_dump($msg);
        }

        foreach ($this->clients as $client) {
            if ($client !== $from) {
                $client->send($msg);
            }
        }
    }

    private function getClientIds()
    {
        $clientIds = [];
        foreach ($this->clients as $client) {
            $clientIds[] = $client->resourceId;
        }

        return $clientIds;
    }
}

$server = \Ratchet\Server\IoServer::factory(
    new \Ratchet\Http\HttpServer(
        new \Ratchet\WebSocket\WsServer(
            new Chat()
        )
    ),
    8080
);

$server->run();
